<?php
/*
    Here I chose to create and include an external script "db.php", to reduce
    duplication of code, as connection to database is required in many of the next assignments
*/
require_once('db.php');

// Start session
session_start();

// Check if user is trying to log out
if (isset($_GET['logout'])) {
    session_destroy();
    // Refresh page and exit script
    header('Location: ' . $_SERVER['PHP_SELF']);
    exit();
}

// Check if the user is trying to log in
if (isset($_POST['username']) && isset($_POST['password'])) {
    $db = connectDB();
    
    try {
        $stmt = $db->prepare('SELECT id, uname, pwd FROM user WHERE uname=?');
        $stmt->execute(array($_POST['username']));
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        echo $e->getMessage;
    }
    
    // Verify if password is correct, then show welcome page or
    // show login form again
    if (password_verify($_POST['password'], $result['pwd'])) {
        // Set session variable for user
        $_SESSION['uid'] = $result['id'];
        // Refresh page to show welcome page
        header("Refresh:0");
        exit();
    } else {
        require_once('loginform.php');
        echo '<b>Wrong username/password</b>';
    }
} else {
    // Check if the user already has a logged in session
    if (isset($_SESSION['uid'])) {
        $db = connectDB();
        try {
            $stmt = $db->prepare('SELECT id, uname FROM user WHERE id=?');
            $stmt->execute(array($_SESSION['uid']));
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            echo $e->getMessage;
        }
    
        // Check if we get empty result set or not (user exists or not)
        if ($result) {
            require_once('welcomepage.php');
        } else {
            die('Invalid session');
        }
    } else {
        require_once('loginform.php');
    }
}
?>