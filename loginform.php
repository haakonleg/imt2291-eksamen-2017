<form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>" method="POST">
    <label>Username</label>
    <input type="text" name="username" value="<?php if (isset($_POST['username'])) echo $_POST['username'] ?>" required>
    <br>
    <label>Password</label>
    <input type="password" name="password" required>
    <br>
    <input type="submit" value="Login">
</form>