<?php
require_once('db.php');

/*
    Fetches all study programs from the database
    Return: array of IDs corresponding to every study program
*/
function getStudyPrograms() {
    $db = connectDB();
    
    try {
        $stmt = $db->prepare('SELECT id, name FROM studyprogram ORDER BY id');
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

/*
    Fetches every study subject within a certain study program
    
    @param programID: the study program ID to get subjects from
    Return: array with information about every subject
*/
function getProgramContent($programID) {
    $db = connectDB();
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    try {
        // Used inner join with the subject table to add additional information about the subject
        // Distinct is used to remove duplicates
        $stmt = $db->prepare('SELECT DISTINCT subject, studyprogramcontent.semester, type, name, credits FROM studyprogramcontent
                               INNER JOIN subject ON studyprogramcontent.subject = subject.code
                               WHERE studyprogram=? ORDER BY semester');
        $stmt->execute(array($programID));
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

// Iterate through every study program and print a new table for each one
foreach(getStudyPrograms() as $program) {
    echo '
        <h2>' . $program['name'] . '</h2>
        <table>
            <thead>
                <tr>
                    <th>Emnekode</th>
                    <th>Emnenavn</th>
                    <th>O/V</th>
                    <th>S1(h)</th>
                    <th>S2(v)</th>
                    <th>S3(h)</th>
                    <th>S4(v)</th>
                    <th>S5(h)</th>
                    <th>S6(v)</th>
                </tr>
            </thead>
            <tbody>
    ';
    // Iterate through every study subject within the current study program
    foreach(getProgramContent($program['id']) as $subject) {
        // Get shorthand subject status
        $type = ($subject['type'] == 'obligatory' ? 'O' : 'V');
        
        // Initialize a two dimensional array with the index corresponding to
        // how many points the subject has in that semester
        $points = array(1 => '', 2 => '', 3 => '', 4 => '', 5 => '', 6 => '');
        // Set the semester/index to amount of points for that semester
        $points[(int)$subject['semester']] = $subject['credits'];

        echo '
            <tr>
                <td>' . $subject['subject']. '</td>
                <td>' . $subject['name'] .'</td>
                <td>' . $type . '</td>
                <td>' . $points[1] . '</td>
                <td>' . $points[2] . '</td>
                <td>' . $points[3] . '</td>
                <td>' . $points[4] . '</td>
                <td>' . $points[5] . '</td>
                <td>' . $points[6] . '</td>
            </tr>
        ';
    }
    echo '</tbody></table>';
}
?>