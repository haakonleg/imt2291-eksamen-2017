<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <title>Upload avatar</title>
</head>
<body>
<div class="container">
<h4>Upload Avatar</h4>
    <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>" method="POST" enctype="multipart/form-data">
        <input type="file" name="avatar">
        <button type="submit" class="btn btn-primary btn-block" name="file">Upload</button>
    </form>
</div>
</body>
</html>
<?php
// Restrict access to the page only to logged in users
session_start();
if (!isset($_SESSION['uid'])) {
    die('<b>Error: You are not logged in.</b>');
}

// If file has been transferred to server
if (isset($_POST['file'])) {
    /*
        This function validates the file by performing several checks, first it checks
        if the image has a legal file extension (JPEG), then it checks whether the file
        really is an image by calling a function to get image dimensions
        
        @param imageFile the image file handle to validate
        
        Return: true/false on succsessfull/failed validation
    */
    function validateImage($imageFile) {
        // Check that file extension is JPEG
        $extension = strtolower((pathinfo($imageFile['name'], PATHINFO_EXTENSION)));
        if ($extension != 'jpg') {
            echo '<b>Error: Only JPEG file type is allowed.</b>';
            return false;
        }
        
        // Check that file is really an image
        if (!getimagesize($imageFile['tmp_name'])) {
            echo '<b>Error: File does not seem to be an image</b>';
            return false;
        }
        
        // Check that file does not exceed max size (1MB)
        if ($imageFile['size'] > 1000000) {
            echo '<b>Error: File exceeds maximum allowed size (1Mb)</b>';
            return false;
        }
        return true;
    }
    
    // If the image is validated, try to upload the image
    if (validateImage($_FILES['avatar'])) {
        require_once('db.php');
        $db = connectDB();
        
        // Resize image to 150x150
        $img = resize_image($_FILES['avatar']['tmp_name'], 150, 150);
        
        // Overwrite the temporary image with the resized image
        imagejpeg($img, $_FILES['avatar']['tmp_name']);
        
        // Read image binary data
        $imagedata = file_get_contents($_FILES['avatar']['tmp_name']);

        // Try to update the users row with the image data
        try {
            $stmt = $db->prepare('UPDATE user SET avatar=:file WHERE id=:userid');
            $stmt->execute(array(':file' => $imagedata, ':userid' => $_SESSION['uid']));
        } catch (PDOException $e) {
            die('<b>Error: Could not upload image</b>');
        }
        echo '<b>Image uploaded successfully.</b>';
    }
}

/*
    Function to resize a JPEG image using the GD library in PHP
    THIS FUNCTION HAS BEEN COPIED FROM HERE: https://stackoverflow.com/a/14649689
*/
function resize_image($file, $w, $h, $crop=FALSE) {
    list($width, $height) = getimagesize($file);
    $r = $width / $height;
    if ($crop) {
        if ($width > $height) {
            $width = ceil($width-($width*abs($r-$w/$h)));
        } else {
            $height = ceil($height-($height*abs($r-$w/$h)));
        }
        $newwidth = $w;
        $newheight = $h;
    } else {
        if ($w/$h > $r) {
            $newwidth = $h*$r;
            $newheight = $h;
        } else {
            $newheight = $w/$r;
            $newwidth = $w;
        }
    }
    $src = imagecreatefromjpeg($file);
    $dst = imagecreatetruecolor($newwidth, $newheight);
    imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

    return $dst;
}
?>