<?php
/*
    This script creates a PDO handle to the database and returns it
    to the caller of the script
    
    Return: PDO handle to database
*/
function connectDB() {
    // Database config
    $SERVER = "localhost";
    $USERNAME = "root";
    $PASSWORD = "";
    $DBNAME = "IMT2291Eksamen471177";
    
    // Try to get database handle and return it
    try {
        $db = new PDO("mysql:host=$SERVER;dbname=$DBNAME;charset=utf8", $USERNAME, $PASSWORD);
        return $db;
    } catch (PDOException $e) {
        die($e->getMessage());
    }
}
?>