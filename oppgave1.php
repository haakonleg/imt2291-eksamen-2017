<?php
// Database configuration
$SERVER = "localhost";
$USERNAME = "root";
$PASSWORD = "";
$DBNAME = "IMT2291Eksamen471177";

// User configuration
$UNAME = "test";
$UPWD = "test";

// Create new database
try {
    $db = new PDO("mysql:host=$SERVER;", $USERNAME, $PASSWORD);
    // Set attribute of PDO to show exceptions
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    // Drop database if it exists (overwrite the database)
    $sql = "DROP DATABASE IF EXISTS $DBNAME";
    $db->exec($sql);
    
    // Create new database
    $sql = "CREATE DATABASE $DBNAME";
    $db->exec($sql);
    
    echo 'Ny database opprettet.<br>';
} catch (PDOException $e) {
    echo $e->getMessage();
}

// Create new user in the new database
try {   
    $db = new PDO("mysql:host=$SERVER;dbname=$DBNAME", $USERNAME, $PASSWORD);
    
    // Create user table
    $stmt = $db->prepare('
        CREATE TABLE user (
            id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
            uname varchar(32) NOT NULL UNIQUE,
            pwd varchar(255) NOT NULL,
            avatar blob
        )
    ');
    $stmt->execute();
    
    // Add user to table
    $stmt = $db->prepare('INSERT INTO user(uname, pwd) VALUES (?, ?)');
    $stmt->execute(array($UNAME, password_hash($UPWD, PASSWORD_DEFAULT)));
    echo 'Ny Bruker opprettet.';
} catch (PDOException $e) {
    echo $e->getMessage();
}
?>