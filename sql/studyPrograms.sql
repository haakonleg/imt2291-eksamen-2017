CREATE TABLE `studyprogram` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `studyprogram`
--

INSERT INTO `studyprogram` (`id`, `name`) VALUES
(1, 'Programmering [ Spill|Applikasjoner ]');

-- --------------------------------------------------------

--
-- Table structure for table `studyprogramContent`
--

CREATE TABLE `studyprogramContent` (
  `studyprogram` bigint(20) NOT NULL,
  `startYear` int(11) NOT NULL COMMENT 'The start year of the study program',
  `subject` varchar(16) NOT NULL COMMENT 'The subject code',
  `semester` smallint(6) NOT NULL COMMENT 'Semester 1 to -',
  `type` enum('obligatory','elective') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `studyprogramContent`
--

INSERT INTO `studyprogramContent` (`studyprogram`, `startYear`, `subject`, `semester`, `type`) VALUES
(1, 2016, 'IMT1031', 1, 'obligatory'),
(1, 2016, 'IMT1082', 2, 'obligatory'),
(1, 2016, 'IMT1362', 1, 'obligatory'),
(1, 2016, 'IMT2021', 3, 'obligatory'),
(1, 2016, 'IMT2243', 2, 'obligatory'),
(1, 2016, 'IMT2282', 4, 'obligatory'),
(1, 2016, 'IMT2291', 4, 'obligatory'),
(1, 2016, 'IMT2571', 3, 'obligatory'),
(1, 2016, 'IMT2581(f)', 5, 'obligatory'),
(1, 2016, 'IMT2581(s)', 6, 'obligatory'),
(1, 2016, 'IMT2681', 3, 'obligatory'),
(1, 2016, 'IMT3110', 5, 'obligatory'),
(1, 2016, 'IMT3281', 5, 'obligatory'),
(1, 2016, 'IMT3501', 5, 'obligatory'),
(1, 2016, 'IMT3602', 6, 'obligatory'),
(1, 2016, 'IMT3673', 4, 'obligatory'),
(1, 2016, 'IMT3912', 6, 'obligatory'),
(1, 2016, 'REA1101', 1, 'obligatory'),
(1, 2016, 'REA1121', 2, 'obligatory'),
(1, 2017, 'IMT1031', 1, 'obligatory'),
(1, 2017, 'IMT1082', 2, 'obligatory'),
(1, 2017, 'IMT1362', 1, 'obligatory'),
(1, 2017, 'IMT2021', 3, 'obligatory'),
(1, 2017, 'IMT2243', 2, 'obligatory'),
(1, 2017, 'IMT2282', 4, 'obligatory'),
(1, 2017, 'IMT2291', 4, 'obligatory'),
(1, 2017, 'IMT2571', 3, 'obligatory'),
(1, 2017, 'IMT2581(f)', 5, 'obligatory'),
(1, 2017, 'IMT2581(s)', 6, 'obligatory'),
(1, 2017, 'IMT2681', 3, 'obligatory'),
(1, 2017, 'IMT3110', 5, 'obligatory'),
(1, 2017, 'IMT3281', 5, 'obligatory'),
(1, 2017, 'IMT3501', 5, 'obligatory'),
(1, 2017, 'IMT3602', 6, 'obligatory'),
(1, 2017, 'IMT3673', 4, 'obligatory'),
(1, 2017, 'IMT3912', 6, 'obligatory'),
(1, 2017, 'REA1101', 1, 'obligatory'),
(1, 2017, 'REA1121', 2, 'obligatory');

-- --------------------------------------------------------

--
-- Table structure for table `subject`
--

CREATE TABLE `subject` (
  `code` varchar(16) NOT NULL,
  `name` varchar(255) NOT NULL,
  `year` int(11) NOT NULL COMMENT 'This is the year the subject is/was given',
  `credits` decimal(10,1) NOT NULL COMMENT 'Number of credits/study points',
  `semester` enum('spring','fall','summer') NOT NULL,
  `url` varchar(255) NOT NULL COMMENT 'Where to find information about the subject'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subject`
--

INSERT INTO `subject` (`code`, `name`, `year`, `credits`, `semester`, `url`) VALUES
('IMT1031', 'Grunnleggende programmering', 2015, '10.0', 'fall', 'https://www.ntnu.no/studier/emner/IMT1031/2016'),
('IMT1031', 'Grunnleggende programmering', 2016, '10.0', 'fall', 'https://www.ntnu.no/studier/emner/IMT1031/2016'),
('IMT1082', 'Objekt-orientert programmering', 2016, '10.0', 'spring', 'https://www.ntnu.no/studier/emner/IMT1082/2016'),
('IMT1082', 'Objekt-orientert programmering', 2017, '10.0', 'spring', 'https://www.ntnu.no/studier/emner/IMT1082/2016'),
('IMT1362', 'Opplevelsesdesign', 2016, '10.0', 'fall', 'http://www.ntnu.edu/studies/courses/IMT1362/2016'),
('IMT1362', 'Opplevelsesdesign', 2017, '10.0', 'fall', 'http://www.ntnu.edu/studies/courses/IMT1362/2016'),
('IMT2021', 'Algoritmiske metoder', 2016, '10.0', 'fall', 'https://www.ntnu.no/studier/emner/IMT2021/2016'),
('IMT2021', 'Algoritmiske metoder', 2017, '10.0', 'fall', 'https://www.ntnu.no/studier/emner/IMT2021/2016'),
('IMT2243', 'Systemutvikling', 2016, '10.0', 'spring', 'https://www.ntnu.no/studier/emner/IMT2243/2016'),
('IMT2243', 'Systemutvikling', 2017, '10.0', 'spring', 'https://www.ntnu.no/studier/emner/IMT2243/2016'),
('IMT2282', 'Operativsystemer', 2017, '10.0', 'spring', 'https://www.ntnu.no/studier/emner/IMT2282/2016'),
('IMT2282', 'Operativsystemer', 2018, '10.0', 'spring', 'https://www.ntnu.no/studier/emner/IMT2282/2016'),
('IMT2291', 'WWW-Teknologi', 2018, '10.0', 'spring', 'https://www.ntnu.no/studier/emner/IMT2291/2016'),
('IMT2291', 'WWW-Teknologi', 2019, '10.0', 'spring', 'https://www.ntnu.no/studier/emner/IMT2291/2016'),
('IMT2571', 'Datamodellering og databasesystemer', 2017, '10.0', 'fall', 'https://www.ntnu.no/studier/emner/IMT2571/2016'),
('IMT2571', 'Datamodellering og databasesystemer', 2018, '10.0', 'fall', 'https://www.ntnu.no/studier/emner/IMT2571/2016'),
('IMT2581(f)', 'Rask prototyping og innovasjon', 2017, '2.5', 'fall', 'https://www.ntnu.no/studier/emner/IMT2581/2016'),
('IMT2581(f)', 'Rask prototyping og innovasjon', 2018, '2.5', 'fall', 'https://www.ntnu.no/studier/emner/IMT2581/2016'),
('IMT2581(s)', 'Rask prototyping og innovasjon', 2018, '2.5', 'spring', 'https://www.ntnu.no/studier/emner/IMT2581/2016'),
('IMT2581(s)', 'Rask prototyping og innovasjon', 2019, '2.5', 'spring', 'https://www.ntnu.no/studier/emner/IMT2581/2016'),
('IMT2681', 'Cloud Technologies', 2017, '10.0', 'fall', 'https://www.ntnu.no/studier/emner/IMT2681/2016'),
('IMT2681', 'Cloud Technologies', 2018, '10.0', 'fall', 'https://www.ntnu.no/studier/emner/IMT2681/2016'),
('IMT3110', 'Programvaremønstre', 2017, '10.0', 'fall', 'https://www.ntnu.no/studier/emner/IMT3110/2016'),
('IMT3110', 'Programvaremønstre', 2018, '10.0', 'fall', 'https://www.ntnu.no/studier/emner/IMT3110/2016'),
('IMT3281', 'Applikasjonsutvikling', 2018, '10.0', 'fall', 'https://www.ntnu.no/studier/emner/IMT3281/2016'),
('IMT3281', 'Applikasjonsutvikling', 2019, '10.0', 'fall', 'https://www.ntnu.no/studier/emner/IMT3281/2016'),
('IMT3501', 'Programvaresikkerhet', 2017, '10.0', 'fall', 'https://www.ntnu.no/studier/emner/IMT3501/2016'),
('IMT3501', 'Programvaresikkerhet', 2018, '10.0', 'fall', 'https://www.ntnu.no/studier/emner/IMT3501/2016'),
('IMT3602', 'Professional Programming', 2019, '5.0', 'spring', 'https://www.ntnu.no/studier/emner/IMT3602/2016'),
('IMT3602', 'Professional Programming', 2020, '5.0', 'spring', 'https://www.ntnu.no/studier/emner/IMT3602/2016'),
('IMT3673', 'Mobile/Wearable Programming', 2017, '10.0', 'spring', 'https://www.ntnu.no/studier/emner/IMT3673/2016'),
('IMT3673', 'Mobile/Wearable Programming', 2018, '10.0', 'spring', 'https://www.ntnu.no/studier/emner/IMT3673/2016'),
('IMT3912', 'Bacheloroppgave', 2018, '20.0', 'spring', 'https://www.ntnu.no/studier/emner/IMT3912/2016'),
('IMT3912', 'Bacheloroppgave', 2019, '20.0', 'spring', 'https://www.ntnu.no/studier/emner/IMT3912/2016'),
('REA1101', 'Matematikk for informatikkfag', 2016, '10.0', 'fall', 'https://www.ntnu.no/studier/emner/REA1101/2016'),
('REA1101', 'Matematikk for informatikkfag', 2017, '10.0', 'fall', 'https://www.ntnu.no/studier/emner/REA1101/2016'),
('REA1121', 'Matematikk for programmering', 2016, '10.0', 'spring', 'https://www.ntnu.no/studier/emner/REA1121/2016'),
('REA1121', 'Matematikk for programmering', 2017, '10.0', 'spring', 'https://www.ntnu.no/studier/emner/REA1121/2016');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `studyprogram`
--
ALTER TABLE `studyprogram`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `studyprogramContent`
--
ALTER TABLE `studyprogramContent`
  ADD KEY `studyprogram` (`studyprogram`),
  ADD KEY `startYear` (`startYear`),
  ADD KEY `subject` (`subject`);

--
-- Indexes for table `subject`
--
ALTER TABLE `subject`
  ADD PRIMARY KEY (`code`,`year`,`semester`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `studyprogram`
--
ALTER TABLE `studyprogram`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
