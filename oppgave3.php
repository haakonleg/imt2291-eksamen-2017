<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <title>Create New User</title>
</head>
<body>
<div class="container">
    <h4>New User</h4>
    <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>" method="POST">
        <div class="form-group">
            <label for="username">Username</label>
            <input type="text" class="form-control" id="username" name="username" value="<?php if (isset($_POST['username'])) echo $_POST['username'] ?>" required>
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" id="password" name="password" required>
        </div>
        <div class="form-group">
            <label for="password2">Confirm Password</label>
            <input type="password" class="form-control" id="password2" name="password2" required>
        </div>
        <button type="submit" class="btn btn-primary btn-block">Create User</button>
    </form>
    <div id="errormsg" class="alert alert-danger" style="display: none;"></div>
</div>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script>
// Get form
$form = $('form');
// Get error message div
$errormsg = $('div[id="errormsg"]');

// Submit handler to validate form data before submit
// only submit the form if it is validated
$form.submit( function(event) {
    // Check that username is 6 characters long or more
    if ($form.find('input[name="username"]').val().length < 6) {
        $errormsg.html('Username must be 6 characters or more').slideDown('fast');
        return false;
    }
        
    // Check that password is 8 characters long or more
    if ($form.find('input[name="password"]').val().length < 8) {
        $errormsg.html('Password must be 8 characters or more').slideDown('fast');
        return false;
    }
    
    // Check that both passwords match
    if ($form.find('input[name="password"]').val() != $form.find('input[name="password2"]').val()) {
        $errormsg.html('Password do not match').slideDown('fast');
        return false;
    }
});

// Hide the error message on form focus
$form.find('input').focus(function () {
    $('.alert').slideUp('fast');
});
</script>
</body>
</html>
<?php
    // This PHP script runs when the form has been successfully submitted/validated
    // and will attempt to add the new user to the database
    
    if ( isset($_POST['username']) && isset($_POST['password']) && isset($_POST['password2']) ) {
        require_once('db.php');
        $db = connectDB();
        
        try {
            // Try to add the new user to the database
            $stmt = $db->prepare('INSERT INTO user(uname, pwd) VALUES(?, ?)');
            if ($stmt->execute(array( htmlspecialchars($_POST['username']), password_hash($_POST['password'], PASSWORD_DEFAULT)))) {
                // Success, set the session variable and redirect to login page
                session_start();
                // Use lastInsertID (which is the user id) as the session variable
                $_SESSION['uid'] = $db->lastInsertID();
                header('Location: oppgave2.php');
                exit();
            } else {
                // Error, the user could not be created, show error message
                echo '<b>User already exists, please choose a different username.</b>';
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
?>