<?php
/*
    This script fetches all users from the database and returns
    userid, username, and boolean for whether they have an avatar image or not
    
    Return: json encoded array with information about all registered users
*/
require_once('db.php');
require_once('avatar.php');

// Connect to database
$db = connectDB();

// Fetch all users
$users = array();
try {
    $db = connectDB();
    $stmt = $db->prepare('SELECT id, uname FROM user ORDER BY id');
    $stmt->execute();
    $users = $stmt->fetchAll(PDO::FETCH_ASSOC);
} catch (PDOException $e) {
    echo $e->getMessage();
}

// Check whether each user has an avatar and add boolean y/n to users array
// $i is used as value for the current array index
$i = 0;
foreach($users as $user) {
    if (getAvatar($user['id'])) {
        $users[$i]['avatar'] = 'y';
    } else {
        $users[$i]['avatar'] = 'n';
    }
    $i++;
}

// Return array encoded in JSON format
// and set content type to JSON
header('Content-Type: application/json');
echo json_encode($users);
?>