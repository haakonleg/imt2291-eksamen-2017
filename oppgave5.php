<?php
require_once('db.php');
// Include avatar.php to be able to check if the user has an avatar or not
require_once('avatar.php');

// Retrieve all users username and userids from the database
$users = array();
try {
    $db = connectDB();
    $stmt = $db->prepare('SELECT id, uname FROM user ORDER BY id');
    $stmt->execute();
    $users = $stmt->fetchAll(PDO::FETCH_ASSOC);
} catch (PDOException $e) {
    echo $e->getMessage();
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <title>User List</title>
</head>
<body>
<div class="container">
    <table class="table">
        <thead>
            <tr>
                <th>Username</th>
                <th>Avatar</th>
            </tr>
        </thead>
        <tbody>
            <?php 
            // For each user in the array, print the username and attempt to print their avatar if they have one
            foreach ($users as $user) {
                echo '<tr>';
                echo '<td>' . $user['uname'] . '</td>';
                // Get result of avatar function
                $avatar = getAvatar($user['id']);
                // If user has avatar
                if ($avatar) {
                    // This line of code to display image has been taken from: https://stackoverflow.com/a/35056782
                    echo '<td><img src="data:image/jpeg;base64,'.base64_encode( $avatar ).'"/>';
                // If user has no avatar
                } else {
                    echo '<td><b>No Avatar</b></td>';
                }
                echo '</tr>';
            }
            ?>
        </tbody>
    </table>
</div>
</body>
</html>