<?php
/*
    This script retrieves a users avatar image from the database using user id
    the binary data of the image is fetched from the database and encoded to image
    
    Return value: bool value of false if user has no avatar, image data returned otherwise
*/

require_once('db.php');

function getAvatar($userid) {
    $db = connectDB();
    try {
        $stmt = $db->prepare('SELECT avatar FROM user WHERE id=?');
        $stmt->execute(array($userid));
        $img = $stmt->fetchColumn();
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
    
    if ($img == NULL)
        return false;
    else {
        // Return the image
        return $img;
    }
}
?>